﻿using System;

namespace ConsoleApp1
{
    class App
    {
        internal void Message(string pText)
        {
            Console.WriteLine(pText);
        }
    }

    public class Result
    {
        public int Code { get; set; }
        public string Msg { get; set; }
    }
}
