﻿namespace ConsoleApp1
{
    public abstract class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public abstract Result Validate();
    }
}