﻿namespace ConsoleApp1
{
    public class CustomerKg : Customer
    {
        public string Inn { get; set; }

        public override Result Validate()
        {
            if (this.Inn.Length != 14)
                return new Result { Code = 1, Msg = "Длина ИНН должно быть 14 символов так как ПО используется в Кыргызстане!" };
            return new Result { Code = 0, Msg = "OK" };
        }
    }
}