﻿namespace ConsoleApp1
{
    public class CustomerTj : Customer
    {
        public string EIN { get; set; }
        public string CIN { get; set; }

        public override Result Validate()
        {
            if (this.EIN.Length != 9)
                return new Result { Code = 1, Msg = "Длина ЕИН должно быть 9 символов так как ПО используется в Таджикистане!" };
            if (this.CIN.Length != 8)
                return new Result { Code = 1, Msg = "Длина CIN должно быть 8 символов так как ПО используется в Таджикистане!" };
            return new Result { Code = 0, Msg = "OK" };
        }
    }
}
