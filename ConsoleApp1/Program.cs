﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerKg customerKg = new CustomerKg();
            customerKg.Id = 1;
            customerKg.Name = "Кыргызстан";
            customerKg.Inn = "1111111111111111";

            CustomerTj customerTj = new CustomerTj();
            customerTj.Id = 2;
            customerTj.Name = "Таджикистан";
            customerTj.EIN = "111111111";
            customerTj.CIN = "11111111123";

            Dictionary<string, Customer> customers = new Dictionary<string, Customer>();
            customers.Add("KG", customerKg);
            customers.Add("TJ", customerTj);

            foreach (var obj in customers)
            {
                Console.WriteLine($"{obj.Key}, - Страна: {obj.Value.Id } - {obj.Value.Name} Результат: {obj.Value.Validate().Code} - {obj.Value.Validate().Msg}\n");
            }
        }
    }
}